
## godot-stuff Entity Component System (ECS)

A Library to provide with Godot with a very simple Entity Component System (ECS) framework.

Official documentation can be found [here.](https://gs-ecs-docs.readthedocs.io/en/latest/)

