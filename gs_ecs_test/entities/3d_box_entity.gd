class_name BoxEntity
extends Entity


func on_ready():

	var _movable = get_component("movable")

	_movable.origin = self.position
	_movable.moveto_pos = self.position + Vector3(1, 1, 0) * _movable.distance
	Logger.debug("- moveto_pos: %s" % [_movable.moveto_pos])

