class_name TestEntity
extends Entity


func on_ready():

	var _movable = get_component("movable")

	_movable.origin = self.position
	_movable.moveto_pos = self.position + Vector2(1, 1) * _movable.distance
#	self.position = _movable.moveto_pos
	Logger.debug("- moveto_pos: %s" % [_movable.moveto_pos])

