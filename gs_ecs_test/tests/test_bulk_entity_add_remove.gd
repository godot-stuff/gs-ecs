extends Node2D

@export var entity: PackedScene

func _ready():
	
	pass
	
	
func _process(delta):
	
	ECS.update()
	

func _on_add_pressed():
	
	for i in range(10):
	
		var _entity = entity.instantiate()
		$Entities.add_child(_entity, true)
		
		var x = randi_range(0,960)
		var y = randi_range(0,540)
		
		var v2 = Vector2(x, y)
		
		_entity.global_position = v2


func _on_remove_pressed():
	
	var count = 0
	
	for child in $Entities.get_children():
		
		_remove_child(child)
		
		count += 1
		
		if count > 9:
			break
		
		
func _on_all_pressed():
	
	for child in $Entities.get_children():
		
		_remove_child(child)


func _remove_child(child):
	
	ECS.remove_entity(child)
#	child.queue_free()
