extends Node2D

@export var entity : PackedScene

var count : int = 100

func _ready():
	
	_on_bulk_timer_timeout()

func _process(delta):
	
	ECS.update()
	
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()

func _on_bulk_timer_timeout():
	
	for child in $Entities.get_children():
		ECS.remove_entity(child)
	
	for i in range(count):
		
		var _entity = entity.instantiate()
		$Entities.add_child(_entity)
		
		var x = randi_range(0,960)
		var y = randi_range(0,540)
		
		var v2 = Vector2(x, y)
		
		_entity.global_position = v2
		

	count += 100
