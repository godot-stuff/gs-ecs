extends Node2D

@export var entity : PackedScene

var count : int = 100
var entity_count : int = 0
var increment : int = 100
var max : int = 2000
var forward : bool = true

func _ready():
	
	pass
	

func _process(delta):
	
	if forward:
		
		
		if entity_count < count:
			
			entity_count += 1
			
			var _entity = entity.instantiate()
			$Entities.add_child(_entity)
			
			var x = randi_range(0,960)
			var y = randi_range(0,540)
			
			var v2 = Vector2(x, y)
			
			_entity.global_position = v2
			
			$Delay.start()
			
	else:
		
		var _children = $Entities.get_children()
		
		if _children.size() > 0:
			
			_children[0].queue_free()
			
		else:
			
			forward = true
			entity_count = 0
			count += increment
			
			if count > max:				
				count = max
			
	
	ECS.update()
	
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
		
	var _other = ""
	
	_other += "entity_count: %s\n" % entity_count
	_other += "count: %s\n" % count
	_other += "increment: %s\n" % increment
	_other += "max: %s\n" % max
	
	$TestEcsInfo.add_info(_other)


func _on_delay_timeout():
	
	forward = false
