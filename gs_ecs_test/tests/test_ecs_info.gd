extends Panel

@onready var debug = $Debug

var other_info = ""

func add_info(text):
	
	other_info = text
	
	
func _process(delta):
	
	var _debug = ""
	
	_debug += "FPS: %3.1f\n" % Engine.get_frames_per_second()
	_debug += "Entities: %s\n" % ECS.entities.size()
	_debug += "Component Entities: %s\n" % ECS.component_entities.size()
	
	for c in ECS.component_entities:
		_debug += "  -> %s: %d\n" % [c, ECS.component_entities[c].size()]
	
	_debug += other_info
	
	debug.text = _debug
	
