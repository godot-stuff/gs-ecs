class_name Movable
extends Component

@export var SPEED: float = 5
@export var SPEED_FACTOR: float = 10
@export var DISTANCE: float = 0


var origin = Vector2()
var distance = 0
var speed = 0
var speed_factor = 10
var moveto_pos = Vector2()
var direction = 1


func _ready():

	if SPEED:			speed = SPEED
	if SPEED_FACTOR:	speed_factor = SPEED_FACTOR
	if DISTANCE:		distance = DISTANCE
